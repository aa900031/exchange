<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    const ID = 'id';
    const ORDER_ID_BUY = 'order_id_buy';
    const ORDER_ID_SELL = 'order_id_sell';
    const AMOUNT = 'amount';
    const PRICE = 'price';

    public function orderBuy()
    {
        return $this->belongsTo(Order::class, 'order_id_buy');
    }

    public function orderSell()
    {
        return $this->belongsTo(Order::class, 'order_id_sell');
    }
}
