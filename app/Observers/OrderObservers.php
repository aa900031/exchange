<?php

namespace App\Observers;

use App\Models\User;
use App\Models\Order;

class OrderObservers
{
    public function saved(Order $order)
    {
        $user = $order->user()->first();

        if ($order->type == Order::TYPE__BUY) {
            $priceWallet = $user->wallets()->where([
                'asset_id' => $order->asset_id_price,
            ])->firstOrFail();

            $priceWallet->freeze += $order->price * $order->amount;
            $priceWallet->save();
        } elseif ($order->type == Order::TYPE__SELL) {
            $targetWallet = $user->wallets()->where([
                'asset_id' => $order->asset_id_target,
            ])->firstOrFail();

            $targetWallet->freeze += $order->amount;
            $targetWallet->save();
        }
    }
}