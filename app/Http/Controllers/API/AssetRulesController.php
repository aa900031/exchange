<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\DB;
use App\Models\Asset;
use App\Models\AssetRule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssetRulesController extends Controller
{
    public function index($id)
    {
        $asset = Asset::find($id);
        $rules = $asset->rules()->get();
        return response()->api($rules);
    }

    public function passed($id, $target_id)
    {
        $asset = Asset::find($id);
        $rule = $asset->rules()->where('asset_id_target', $target_id)->first();
        $passed = $rule && $rule->passed ? true : false;
        
        return response()->api($passed);
    }

    public function uses($id)
    {
        $uses = DB::table('assets')
            ->join('asset_rules', 'asset_rules.asset_id_target', '=', 'assets.id')
            ->where('asset_rules.asset_id', $id)
            ->where('asset_rules.passed', true)
            ->select('assets.*')
            ->get();

        return response()->api($uses);
    }
}
