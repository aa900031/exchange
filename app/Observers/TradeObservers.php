<?php

namespace App\Observers;

use App\Models\Trade;
use App\Jobs\AfterTrade;

class TradeObservers
{
    public function saved(Trade $trade)
    {
        AfterTrade::dispatch($trade);
    }
}