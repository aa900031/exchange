<?php

namespace App\Admin\Controllers;

use App\Models\Trade;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class TradeController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Trade');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit Trade');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create Trade');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Trade::class, function (Grid $grid) {
            $grid->id('ID')->sortable();
            $grid->column('orderBuy.id', 'Buy Order ID');
            $grid->column('orderSell.id', 'Sell Order ID');
            $grid->column('Target Asset ID')->display(function () {
                return $this->orderBuy()->first()
                    ->assetTarget()->first()->id;
            });
            $grid->column('Target Asset Name')->display(function () {
                return $this->orderBuy()->first()
                    ->assetTarget()->first()->name;
            });
            $grid->column('Price Asset ID')->display(function () {
                return $this->orderBuy()->first()
                    ->assetPrice()->first()->id;
            });
            $grid->column('Price Asset Name')->display(function () {
                return $this->orderBuy()->first()
                    ->assetPrice()->first()->name;
            });
            $grid->column('amount', 'Amount');
            $grid->column('price', 'Price');

            $grid->created_at();
            $grid->updated_at();

            $grid->disableCreation();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Trade::class, function (Form $form) {
            
        });
    }
}
