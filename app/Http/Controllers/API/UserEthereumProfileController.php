<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserEthereumProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $ethereum = $user->ethereum()->first();

        return response()->api($ethereum);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        
        $request->validate([
            'address' => 'required',
        ]);

        $data = $request->only([
            'address'
        ]);

        $ethereum = $user->ethereum()->updateOrCreate([], $data);
        
        return response()->api($ethereum);
    }
}
