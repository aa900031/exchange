<?php

namespace App\Services;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class OrderService
{
    const REDIS_NAMESPACE_ORDER_LOGS = 'order_logs';

    public $target_id;
    public $price_id;
    public $redis_logs_namespace;

    public function __construct($target_id, $price_id) {
        $this->target_id = $target_id;
        $this->price_id = $price_id;
        $this->redis_logs_namespace = self::REDIS_NAMESPACE_ORDER_LOGS.
            ":".$target_id.
            ":".$price_id;
    }

    public function getLogs($today = true)
    {
        $logs = Redis::get($this->redis_logs_namespace);

        if (!$logs) {
            $orders = Order::with(['assetTarget', 'assetPrice', 'user'])
                ->whereDate(Order::CREATED_AT , Carbon::today())
                ->orderBy(Order::CREATED_AT, 'desc')
                ->get();
            $logs = $orders->toArray();
            Redis::set($this->redis_logs_namespace, serialize($logs));
        } else {
            $logs = unserialize($logs);
        }

        return $logs;
    }

    public function setLog(Order $order)
    {
        $is_cached = Redis::get($this->redis_logs_namespace) ? true : false;
        $logs = $this->getLogs();

        if ($is_cached) {
            array_unshift($logs, $order->toArray());
            Redis::set($this->redis_logs_namespace, serialize($logs));
        }
    }
}