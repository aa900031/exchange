<?php

namespace App\Http\Controllers\API;

use App\Services\OrderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function logs(Request $request)
    {
        $request->validate([
            'target_id' => 'required',
            'price_id' => 'required',
        ]);

        $order_service = new OrderService($request->target_id, $request->price_id);
        $logs = $order_service->getLogs();

        return response()->api($logs);
    }
}
