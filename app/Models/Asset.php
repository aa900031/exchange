<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    const ID = 'id';
    const NAME = 'name';
    const SYMBOL = 'symbol';
    const DECIMAL = 'decimal';
    const TYPE = 'type';
    const TYPE__PHYSICAL = 'physical';
    const TYPE__VIRTUAL = 'virtual';

    public function rules()
    {
        return $this->hasMany(AssetRule::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function trades()
    {
        return $this->hasMany(Trade::class);
    }
}
