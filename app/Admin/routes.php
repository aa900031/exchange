<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('member/users', UserController::class);
    $router->resource('member/profiles', UserProfileController::class);
    $router->resource('member/bank-profile', UserBankProfileController::class);
    $router->resource('member/ethereum-profile', UserEthereumProfileController::class);
    $router->resource('member/asset-wallet', UserWalletController::class);
    $router->resource('asset/items', AssetController::class);
    $router->resource('asset/rules', AssetRuleController::class);
    $router->resource('exchange/orders', OrderController::class);
    $router->resource('exchange/trades', TradeController::class, ['only' => ['index', 'show']]);

    $router->group([
        'prefix' => 'api',
        'namespace' => 'API',
    ], function ($router) {
        $router->resource('users', 'UserController');
        $router->resource('assets', 'AssetController');
    });
});
