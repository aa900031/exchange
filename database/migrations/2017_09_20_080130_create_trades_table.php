<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id_buy')->unsigned();
            $table->integer('order_id_sell')->unsigned();
            $table->bigInteger('amount');
            $table->bigInteger('price');
            $table->timestamps();
            // $table->foreign('order_id_buy')->references('id')->on('orders');
            // $table->foreign('order_id_sell')->references('id')->on('orders');
            // $table->foreign('asset_id_need')->references('id')->on('assets');
            // $table->foreign('asset_id_base')->references('id')->on('assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
