<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'API'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::get('refresh', 'AuthController@refresh');
    });
    
    Route::group(['prefix' => 'user', 'middleware' => 'jwt.auth'], function () {
        Route::resource('profile', 'UserProfileController', [
            'only' => ['index', 'store'],
        ]);
        Route::resource('bank-profile', 'UserBankProfileController', [
            'only' => ['index', 'store'],
        ]);
        Route::resource('ethereum-profile', 'UserEthereumProfileController', [
            'only' => ['index', 'store'],
        ]);
        Route::resource('wallets', 'UserWalletController', [
            'only' => ['index', 'show'],
        ]);
        Route::resource('orders', 'UserOrdersController', [
            'only' => ['index', 'store', 'show', 'update'],
        ]);
    });

    Route::get('assets', 'AssetsController@index');
    Route::get('asset/{id}/rules', 'AssetRulesController@index');
    Route::get('asset/{id}/passed/{target_id}', 'AssetRulesController@passed');
    Route::get('asset/{id}/uses', 'AssetRulesController@uses');

    Route::get('trades/logs', 'TradesController@logs');
    Route::get('trades/price', 'TradesController@price');
    Route::get('trades/{id}', 'TradesController@show');
    Route::get('orders/logs', 'OrdersController@logs');
});
