<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('asset_id_target')->unsigned();
            $table->integer('asset_id_price')->unsigned();
            $table->enum('type', ['buy', 'sell']);
            $table->bigInteger('amount');
            $table->bigInteger('price');
            $table->enum('status', ['success', 'error', 'cancel', 'pending'])->default('pending');
            $table->timestamps();
            // $table->foreign('user_id')->references('id')->on('users');
            // $table->foreign('asset_id_base')->references('id')->on('assets');
            // $table->foreign('asset_id_need')->references('id')->on('assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
