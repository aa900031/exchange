<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $fillable = [
        'account', 'name', 'bank_code', 'country_code',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
