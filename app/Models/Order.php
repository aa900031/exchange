<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const ID = 'id';
    const USER_ID = 'user_id';
    const ASSET_ID_TARGET = 'asset_id_target';
    const ASSET_ID_PRICE = 'asset_id_price';
    const TYPE = 'type';
    const AMOUNT = 'amount';
    const PRICE = 'price';
    const STATUS = 'status';

    const STATUS__SUCCESS = 'success';
    const STATUS__ERROR = 'error';
    const STATUS__CANCEL = 'cancel';
    const STATUS__PENGIND = 'pending';
    const TYPE__BUY = 'buy';
    const TYPE__SELL = 'sell';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function assetTarget()
    {
        return $this->belongsTo(Asset::class, 'asset_id_target');
    }

    public function assetPrice()
    {
        return $this->belongsTo(Asset::class, 'asset_id_price');
    }

    public function trades()
    {
        return $this->hasMany(Trade::class);
    }
}
