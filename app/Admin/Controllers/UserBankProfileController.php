<?php

namespace App\Admin\Controllers;

use App\Models\User;
use App\Models\BankAccount;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class UserBankProfileController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Bank Profile');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit Bank Profile');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create Bank Profile');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(BankAccount::class, function (Grid $grid) {
            $grid->column('user_id', 'User ID')->sortable();
            $grid->column('user.name', 'User Name');
            $grid->column('user.email', 'User Email');
            $grid->column('bank_code', 'Bank Code');
            $grid->column('name', 'Bank Name');
            $grid->column('account', 'Bank Account');
            $grid->column('country_code', 'Country Code');

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(BankAccount::class, function (Form $form) {
            $form->display('id', 'ID');
            $form->select('user_id', 'User')->options(function ($id) {
                $user = User::find($id);

                if ($user) {
                    return [$user->id => $user->name];
                }
            })->ajax('/admin/api/users');
            $form->text('bank_code', 'Bank Code');
            $form->text('name', 'Bank Name');
            $form->text('account', 'Bank Account');
            $form->text('country_code', 'Country Code');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
