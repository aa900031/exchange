<?php

namespace App\Services;

use App\Models\Trade;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class TradeService 
{
    const REDIS_NAMESPACE_TRADE_MAX_PRICE = "trade_max_price";
    const REDIS_NAMESPACE_TRADE_MIN_PRICE = "trade_min_price";
    const REDIS_NAMESPACE_TRADE_LOGS = "trade_logs";

    public $target_id;
    public $price_id;
    public $redis_max_price_namespace;
    public $redis_min_price_namespace;
    public $redis_logs_namespace;

    public function __construct($target_id, $price_id)
    {
        $this->target_id = $target_id;
        $this->price_id = $price_id;

        $this->redis_max_price_namespace = self::REDIS_NAMESPACE_TRADE_MAX_PRICE.
            ":".$target_id.
            ":".$price_id;
        $this->redis_min_price_namespace = self::REDIS_NAMESPACE_TRADE_MIN_PRICE.
            ":".$target_id.
            ":".$price_id;
        $this->redis_logs_namespace = self::REDIS_NAMESPACE_TRADE_LOGS.
            ":".$target_id.
            ":".$price_id;
    }

    public function getLogs($today = true)
    {
        $logs = Redis::get($this->redis_logs_namespace);

        if (!$logs) {
            $trades = Trade::with(['orderBuy.assetTarget', 'orderBuy.user', 'orderSell.assetPrice', 'orderSell.user'])
                ->whereDate(Trade::CREATED_AT, Carbon::today())
                ->orderBy(Trade::CREATED_AT, 'desc')
                ->get();
            $logs = $trades->toArray();
            Redis::set($this->redis_logs_namespace, serialize($logs));
        } else {
            $logs = unserialize($logs);
        }

        return $logs;
    }

    public function getMaxPrice($today = true)
    {
        $max_price = Redis::get($this->redis_max_price_namespace) ?: -1;
        
        // 最高價為 -1，從資料庫取得最高價
        if ($max_price == -1) {
            $max_price = Trade::whereHas('orderBuy', function ($query) {
                    $query->where(Order::ASSET_ID_TARGET, $this->target_id);
                })
                ->whereHas('orderSell', function ($query) {
                    $query->where(Order::ASSET_ID_PRICE , $this->price_id);
                })
                ->whereDate(Trade::CREATED_AT, Carbon::today())
                ->max(Trade::PRICE) ?: 0;
            Redis::set($this->redis_max_price_namespace, $max_price);
        }

        return $max_price;
    }

    public function getMinPrice($today = true)
    {
        $min_price = Redis::get($this->redis_min_price_namespace) ?: -1;

        // 最低價為 -1，從資料庫取得最低價
        if ($min_price == -1) {
            $min_price = Trade::whereHas('orderBuy', function ($query) {
                    $query->where(Order::ASSET_ID_TARGET, $this->target_id);
                })
                ->whereHas('orderSell', function ($query) {
                    $query->where(Order::ASSET_ID_PRICE , $this->price_id);
                })
                ->whereDate(Trade::CREATED_AT, Carbon::today())
                ->min(Trade::PRICE) ?: 0;
            Redis::set($this->redis_min_price_namespace, $min_price);
        }

        return $min_price;
    }

    public function setMaxPrice($price)
    {
        Redis::set($this->redis_max_price_namespace, $price);
    }

    public function setMinPrice($price)
    {
        Redis::set($this->redis_min_price_namespace, $price);
    }

    public function setLog(Trade $trade)
    {
        $is_cached = Redis::get($this->redis_logs_namespace) ? true : false;
        $logs = $this->getLogs();

        if ($is_cached) {
            array_unshift($logs, $trade->toArray());
            Redis::set($this->redis_logs_namespace, serialize($logs));
        }
    }
}