<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetRule extends Model
{
    const ID = 'id';
    const ASSET_ID = 'asset_id';
    const ASSET_ID_TARGET = 'asset_id_target';
    const PASSED = 'passed';

    public function source()
    {
        return $this->belongsTo(Asset::class, 'asset_id');
    }

    public function target()
    {
        return $this->belongsTo(Asset::class, 'asset_id_target');
    }
    

}
