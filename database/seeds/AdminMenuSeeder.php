<?php

use Illuminate\Database\Seeder;
use Encore\Admin\Auth\Database\Menu;

class AdminMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::insert([
            [
                'parent_id' => 0,
                'order' => 8,
                'title' => 'Member',
                'icon' => 'fa-user',
                'uri' => '',
            ],
            [
                'parent_id' => 8,
                'order' => 9,
                'title' => 'Users',
                'icon' => 'fa-users',
                'uri' => 'member/users',
            ],
            [
                'parent_id' => 8,
                'order' => 10,
                'title' => 'Profile',
                'icon' => 'fa-file',
                'uri' => 'member/profiles',
            ],
            [
                'parent_id' => 8,
                'order' => 11,
                'title' => 'Bank Profile',
                'icon' => 'fa-bank',
                'uri' => 'member/bank-profile',
            ],
            [
                'parent_id' => 8,
                'order' => 12,
                'title' => 'Ethereum Profile',
                'icon' => 'fa-bitcoin',
                'uri' => 'member/ethereum-profile',
            ],
            [
                'parent_id' => 8,
                'order' => 13,
                'title' => 'Wallet',
                'icon' => 'fa-money',
                'uri' => 'member/asset-wallet',
            ],
            [
                'parent_id' => 0,
                'order' => 14,
                'title' => 'Asset',
                'icon' => 'fa-building',
                'uri' => '',
            ],
            [
                'parent_id' => 14,
                'order' => 15,
                'title' => 'Assets',
                'icon' => 'fa-archive',
                'uri' => 'asset/items',
            ],
            [
                'parent_id' => 14,
                'order' => 16,
                'title' => 'Rule',
                'icon' => 'fa-book',
                'uri' => 'asset/rules',
            ],
            [
                'parent_id' => 0,
                'order' => 17,
                'title' => 'Exchange',
                'icon' => 'fa-exchange',
                'uri' => '',
            ],
            [
                'parent_id' => 17,
                'order' => 18,
                'title' => 'Order',
                'icon' => 'fa-file-text',
                'uri' => 'exchange/orders',
            ],
            [
                'parent_id' => 17,
                'order' => 19,
                'title' => 'Trade',
                'icon' => 'fa-exchange',
                'uri' => 'exchange/trades',
            ],
        ]);
    }
}
