<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserBankProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $bank = $user->bank()->first();

        return response()->api($bank);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'account' => 'required',
            'name' => 'required',
            'bank_code' => 'required',
            'country_code' => 'required',
        ]);

        $data = $request->only([
            'account', 'name', 'bank_code', 'country_code',
        ]);

        $bank = $user->bank()->updateOrCreate([], $data);
        
        return response()->api($bank);
    }
}
