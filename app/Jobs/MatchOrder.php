<?php

namespace App\Jobs;

use App\Models\Order;
use App\Models\Trade;
use App\Models\AssetRule;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class MatchOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order_sell;
    public $order_buy;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $now = now();

        $this->order_buy = Order::where(Order::STATUS, Order::STATUS__PENGIND)
            ->where(Order::TYPE, Order::TYPE__BUY)
            ->where(Order::AMOUNT, '>', 0)
            ->where(Order::CREATED_AT, '<=', $now)
            ->orderBy(Order::CREATED_AT)
            ->orderByDesc(Order::PRICE)
            ->get();

        $this->order_sell = Order::where(Order::STATUS, Order::STATUS__PENGIND)
            ->where(Order::TYPE, Order::TYPE__SELL)
            ->where(Order::AMOUNT, '>', 0)
            ->where(Order::CREATED_AT, '<=', $now)
            ->orderBy(Order::CREATED_AT)
            ->orderBy(Order::PRICE)
            ->get();

        $this->order_buy->each(function ($buy) {
            if (!$this->couldTrade($buy->asset_id_target, $buy->asset_id_price)) {
                return false;
            }

            $this->order_sell->filter(function ($sell) use ($buy) {
                return $sell->amount > 0 &&
                   $sell->asset_id_target == $buy->asset_id_target &&
                   $sell->asset_id_price == $buy->asset_id_price &&
                   $sell->price <= $buy->price;
            })->each(function ($sell) use ($buy) {
                DB::transaction(function () use ($sell, $buy) {
                    $buy_amount = $buy->amount;
                    $sell_amount = $sell->amount;
                    $amount = 0;

                    if ($buy_amount >= $sell_amount) {
                        $buy->amount -= $sell_amount;
                        $sell->amount = 0;
                        $sell->status = Order::STATUS__SUCCESS;
                        $amount = $sell_amount;
                    } else {
                        $sell->amount -= $buy_amount;
                        $buy->amount = 0;
                        $buy->status = Order::STATUS__SUCCESS;
                        $amount = $buy_amount;
                    }
                    $buy->save();
                    $sell->save();

                    $trade = new Trade;
                    // 新增 交易結果
                    $trade->order_id_buy = $buy->id;
                    $trade->order_id_sell = $sell->id;
                    $trade->amount = $amount;
                    $trade->price = $sell->price;
                    $trade->save();
                    
                    // 更新買方資產餘額
                    $buyer = $buy->user()->first();
                    $buyerPriceWallet = $buyer->wallet()->where([
                        'asset_id' => $buy->asset_id_price,
                    ])->firstOrFail();
                    $buyerTargetWallet = $buyer->wallet()->firstOrCreate(
                        ['asset_id' => $buy->asset_id_target],
                        ['total' => 0, 'freeze' => 0]
                    );
                    $buyerPriceWallet->freeze -= $buy->price * $amount;
                    $buyerPriceWallet->total -= $sell->price * $amount;
                    $buyerPriceWallet->save();
                    $buyerTargetWallet->total += $amount;
                    $buyerTargetWallet->save();

                    // 更新 賣方資產餘額
                    $seller = $sell->user()->first();
                    $sellerPriceWallet = $seller->wallet()->firstOrCreate(
                        ['asset_id' => $sell->asset_id_price],
                        ['total' => 0, 'freeze' => 0]
                    );
                    $sellerTargetWallet = $seller->wallet()->where([
                        'asset_id' => $sell->asset_id_target
                    ])->firstOrFail();
                    $sellerPriceWallet->total += $sell->price * $amount;
                    $sellerPriceWallet->save();
                    $sellerTargetWallet->freeze -= $amount;
                    $sellerTargetWallet->total -= $amount;
                    $sellerTargetWallet->save();

                });
            });
        });
    }

    private function couldTrade($target_id, $price_id)
    {
        $rule = AssetRule::where('asset_id', $target_id)
            ->where('asset_id_target', $price_id)
            ->first();
        return $rule ? $rule->passed : false;
    }
}
